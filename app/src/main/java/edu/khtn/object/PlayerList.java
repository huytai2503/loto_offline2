package edu.khtn.object;

import java.io.Serializable;
import java.util.ArrayList;

public class PlayerList implements Serializable{
    ArrayList<Player> playerList = new ArrayList<>();

    public PlayerList() {
        super();
    }

    public PlayerList(ArrayList<Player> playerList) {
        this.playerList = playerList;
    }

    public ArrayList<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(ArrayList<Player> playerList) {
        this.playerList = playerList;
    }

    public void addPlayer(Player player) {
        this.playerList.add(player);
    }

    public int size(){
        return playerList.size();
    }

    @Override
    public String toString() {
        return "{" + playerList + '}';
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}