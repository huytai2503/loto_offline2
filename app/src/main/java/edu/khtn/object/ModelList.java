package edu.khtn.object;

import java.io.Serializable;
import java.util.HashMap;

public class ModelList implements Serializable {
    HashMap<Integer, Model> modelList = new HashMap<>();

    public ModelList() {
        super();
    }

    public ModelList(HashMap<Integer, Model> modelList) {
        this.modelList = modelList;
    }

    public HashMap<Integer, Model> getModelList() {
        return modelList;
    }

    public void setModelList(HashMap<Integer, Model> modelList) {
        this.modelList = modelList;
    }

    public void addModel(int key, Model model) {
        this.modelList.put(key, model);
    }

    public int size() {
        return modelList.size();
    }

    @Override
    public String toString() {
        return "{" + modelList + '}';
    }

    public boolean containsKey(int key) {
        return this.modelList.containsKey(key);
    }
}
