package edu.khtn.object;

import java.io.Serializable;
import java.util.Arrays;

public class Model implements Serializable{
    int[][] model = new int[9][9];

    public Model() {
        super();
    }

    public Model(int[][] model) {
        this.model = model;
    }

    public int[][] getModel() {
        return model;
    }

    public void setModel(int[][] model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "(" + Arrays.toString(model) + ')';
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
