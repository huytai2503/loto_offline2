package edu.khtn.loto_offline;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import edu.khtn.object.Model;
import edu.khtn.object.Player;
import edu.khtn.object.PlayerList;

public class PlayerActivity extends Activity implements View.OnClickListener {
    PlayerList playerList = new PlayerList();
    TextView[][] textList = new TextView[9][9];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        linkView();
        setOnClick();
        Intent intent = getIntent();
        Model model = (Model) intent.getSerializableExtra("model");
        int[][] numList = model.getModel();
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                if (numList[r][c] > 0)
                    textList[r][c].setText(numList[r][c] + "");
                else
                    textList[r][c].setText("");
            }
        }
    }

    public void setOnClick() {
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                textList[r][c].setOnClickListener(this);
            }
        }
    }

    public void linkView() {
        textList[0][0] = (TextView) findViewById(R.id.text_0_0);
        textList[0][1] = (TextView) findViewById(R.id.text_0_1);
        textList[0][2] = (TextView) findViewById(R.id.text_0_2);
        textList[0][3] = (TextView) findViewById(R.id.text_0_3);
        textList[0][4] = (TextView) findViewById(R.id.text_0_4);
        textList[0][5] = (TextView) findViewById(R.id.text_0_5);
        textList[0][6] = (TextView) findViewById(R.id.text_0_6);
        textList[0][7] = (TextView) findViewById(R.id.text_0_7);
        textList[0][8] = (TextView) findViewById(R.id.text_0_8);
        textList[1][0] = (TextView) findViewById(R.id.text_1_0);
        textList[1][1] = (TextView) findViewById(R.id.text_1_1);
        textList[1][2] = (TextView) findViewById(R.id.text_1_2);
        textList[1][3] = (TextView) findViewById(R.id.text_1_3);
        textList[1][4] = (TextView) findViewById(R.id.text_1_4);
        textList[1][5] = (TextView) findViewById(R.id.text_1_5);
        textList[1][6] = (TextView) findViewById(R.id.text_1_6);
        textList[1][7] = (TextView) findViewById(R.id.text_1_7);
        textList[1][8] = (TextView) findViewById(R.id.text_1_8);
        textList[2][0] = (TextView) findViewById(R.id.text_2_0);
        textList[2][1] = (TextView) findViewById(R.id.text_2_1);
        textList[2][2] = (TextView) findViewById(R.id.text_2_2);
        textList[2][3] = (TextView) findViewById(R.id.text_2_3);
        textList[2][4] = (TextView) findViewById(R.id.text_2_4);
        textList[2][5] = (TextView) findViewById(R.id.text_2_5);
        textList[2][6] = (TextView) findViewById(R.id.text_2_6);
        textList[2][7] = (TextView) findViewById(R.id.text_2_7);
        textList[2][8] = (TextView) findViewById(R.id.text_2_8);
        textList[3][0] = (TextView) findViewById(R.id.text_3_0);
        textList[3][1] = (TextView) findViewById(R.id.text_3_1);
        textList[3][2] = (TextView) findViewById(R.id.text_3_2);
        textList[3][3] = (TextView) findViewById(R.id.text_3_3);
        textList[3][4] = (TextView) findViewById(R.id.text_3_4);
        textList[3][5] = (TextView) findViewById(R.id.text_3_5);
        textList[3][6] = (TextView) findViewById(R.id.text_3_6);
        textList[3][7] = (TextView) findViewById(R.id.text_3_7);
        textList[3][8] = (TextView) findViewById(R.id.text_3_8);
        textList[4][0] = (TextView) findViewById(R.id.text_4_0);
        textList[4][1] = (TextView) findViewById(R.id.text_4_1);
        textList[4][2] = (TextView) findViewById(R.id.text_4_2);
        textList[4][3] = (TextView) findViewById(R.id.text_4_3);
        textList[4][4] = (TextView) findViewById(R.id.text_4_4);
        textList[4][5] = (TextView) findViewById(R.id.text_4_5);
        textList[4][6] = (TextView) findViewById(R.id.text_4_6);
        textList[4][7] = (TextView) findViewById(R.id.text_4_7);
        textList[4][8] = (TextView) findViewById(R.id.text_4_8);
        textList[5][0] = (TextView) findViewById(R.id.text_5_0);
        textList[5][1] = (TextView) findViewById(R.id.text_5_1);
        textList[5][2] = (TextView) findViewById(R.id.text_5_2);
        textList[5][3] = (TextView) findViewById(R.id.text_5_3);
        textList[5][4] = (TextView) findViewById(R.id.text_5_4);
        textList[5][5] = (TextView) findViewById(R.id.text_5_5);
        textList[5][6] = (TextView) findViewById(R.id.text_5_6);
        textList[5][7] = (TextView) findViewById(R.id.text_5_7);
        textList[5][8] = (TextView) findViewById(R.id.text_5_8);
        textList[6][0] = (TextView) findViewById(R.id.text_6_0);
        textList[6][1] = (TextView) findViewById(R.id.text_6_1);
        textList[6][2] = (TextView) findViewById(R.id.text_6_2);
        textList[6][3] = (TextView) findViewById(R.id.text_6_3);
        textList[6][4] = (TextView) findViewById(R.id.text_6_4);
        textList[6][5] = (TextView) findViewById(R.id.text_6_5);
        textList[6][6] = (TextView) findViewById(R.id.text_6_6);
        textList[6][7] = (TextView) findViewById(R.id.text_6_7);
        textList[6][8] = (TextView) findViewById(R.id.text_6_8);
        textList[7][0] = (TextView) findViewById(R.id.text_7_0);
        textList[7][1] = (TextView) findViewById(R.id.text_7_1);
        textList[7][2] = (TextView) findViewById(R.id.text_7_2);
        textList[7][3] = (TextView) findViewById(R.id.text_7_3);
        textList[7][4] = (TextView) findViewById(R.id.text_7_4);
        textList[7][5] = (TextView) findViewById(R.id.text_7_5);
        textList[7][6] = (TextView) findViewById(R.id.text_7_6);
        textList[7][7] = (TextView) findViewById(R.id.text_7_7);
        textList[7][8] = (TextView) findViewById(R.id.text_7_8);
        textList[8][0] = (TextView) findViewById(R.id.text_8_0);
        textList[8][1] = (TextView) findViewById(R.id.text_8_1);
        textList[8][2] = (TextView) findViewById(R.id.text_8_2);
        textList[8][3] = (TextView) findViewById(R.id.text_8_3);
        textList[8][4] = (TextView) findViewById(R.id.text_8_4);
        textList[8][5] = (TextView) findViewById(R.id.text_8_5);
        textList[8][6] = (TextView) findViewById(R.id.text_8_6);
        textList[8][7] = (TextView) findViewById(R.id.text_8_7);
        textList[8][8] = (TextView) findViewById(R.id.text_8_8);
    }

    @Override
    public void onClick(View v) {
        for (int c = 0; c < 9; c++) {
            for (int r = 0; r < 9; r++) {
                if (textList[c][r] == v) {
                    TransitionDrawable trans =
                            (TransitionDrawable) textList[c][r].getBackground();
                    if (trans.getLevel() == 1)
                        trans.startTransition(50);
                    else
                        trans.reverseTransition(50);
                }
            }
        }
    }

    public void ghiObject(PlayerList playerList) {
        try {
            ObjectOutputStream objOut = new ObjectOutputStream
                    (openFileOutput("ListObject.txt", MODE_PRIVATE));
            objOut.writeObject(playerList);
            objOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PlayerList docObject(String fileName) {
        try {
            ObjectInputStream objIn = new ObjectInputStream
                    (openFileInput("ListObject.txt"));
            PlayerList playerList;
            playerList = (PlayerList) objIn.readObject();
            objIn.close();
            return playerList;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void ghiFile(PlayerList playerList) {
        try {
            PrintWriter wtrOut = new PrintWriter
                    (openFileOutput("ListWrite.txt", MODE_PRIVATE));
            for (Player player : playerList.getPlayerList()) {
                wtrOut.println(player);
            }
            wtrOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public PlayerList docFile() {
        try {
            Scanner wtrIn = new Scanner(openFileInput("ListWrite.txt"));
            PlayerList playerList = new PlayerList();
            while (wtrIn.hasNextLine()) {
                String line = wtrIn.nextLine();
                String[] nameCash = line.split(",");
                Player player = new Player();
                player.setName(nameCash[0]);
                player.setCash(Integer.parseInt(nameCash[1]));
                playerList.addPlayer(player);
            }
            wtrIn.close();
            return playerList;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void thongBao(String noiDung, Boolean exitOrNot) {
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle("Thông Báo!");
        msg.setMessage(noiDung);
        if (exitOrNot.booleanValue()) {
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }
}
