package edu.khtn.loto_offline;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import edu.khtn.object.Model;
import edu.khtn.object.ModelList;

public class ModelsActivity extends Activity implements View.OnClickListener, Serializable {
    Intent intentPlayer;
    Button btnOpenPlayer, btnCreate, btnDelete, btnNext, btnPrev, btnNew;
    ModelList modelList;
    TextView modelKey;
    TextView[][] textViewList = new TextView[9][9];
    String[][] model1 = new String[9][9];
    String[][] model2 = new String[9][9];
    int[][] lctList1 = new int[9][9];
    int[][] lctList2 = new int[9][9];
    int[][] numList1 = new int[5][9];
    int[][] numList2 = new int[5][9];
    int key = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_models);
        intentPlayer = new Intent(this, PlayerActivity.class);
        linkView();
        setOnClick();
        modelKey.setText(key + "");
        modelList = docObject();
        showModel(key);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_openPlayer:
                if (modelList.getModelList().isEmpty()==false &&
                        key < modelList.size()) {
                    Model model = modelList.getModelList().get(key);
                    intentPlayer.putExtra("model", model);
                    startActivity(intentPlayer);
                }
                break;
            case R.id.btn_create:
                createModel();
                break;
            case R.id.btn_delete:
                deleteModel();
                break;
            case R.id.btn_next:
                nextModel();
                break;
            case R.id.btn_prev:
                prevModel();
                break;
            case R.id.btn_autoCreate:
                createNumTable();
                newModel();
                addToModelTable(model1);
                createModel();
                newModel();
                addToModelTable(model2);
                createModel();
                break;
            default:
                break;
        }
    }

    public void addToModelTable(String[][] model){
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                textViewList[r][c].setText(model[r][c]);
            }
        }
    }

    public void createNumTable(){
        rdmNumList();
        splitLocation();
        mergNumListAndLocation(numList1, lctList1, model1);
        mergNumListAndLocation(numList2, lctList2, model2);
        change0to90(model1);
        change0to90(model2);
    }

    public void change0to90(String[][] model){
        for (int r = 0; r < 9; r++) {
            if (model[r][0].equalsIgnoreCase("")==false&&
                    model[r][8].equalsIgnoreCase("")){
                for (int r2 = 0; r2 < 9; r2++) {
                    if(model[r2][0].equalsIgnoreCase("0")){
                        model[r2][0] = model[r][0];
                        model[r][0] = "";
                        model[r][8] = "90";
                    }
                }
            }
        }
    }

    public void mergNumListAndLocation
            (int[][] numList, int[][] lctList,String[][] model) {
        int i = 0;
        for (int c = 0; c < 9; c++) {
            for (int r = 0; r < 9; r++) {
                if (lctList[r][c] == 0) {
                    model[r][c] = "";
                }
                else {
                    model[r][c] = String.valueOf(numList[i][c]);
                    i++;
                }
            }
            i = 0;
        }
    }

    public void splitLocation() {
        lctList1 = rdmLocation();
        lctList2 = rdmLocation();
    }

    public int[][] rdmLocation() {
        int[][] lctList = new int[9][9];
        for (int r = 0; r < 9; r++) {
            int sumPage = 0;
            int sumList = 0;
            int minList = 1;
            do {
                sumList = 0;
                minList = 1;
                sumPage = 0;
                int[] rowList = rdmRow();
                for (int c = 0; c < 9; c++) {
                    lctList[r][c] = rowList[c];
                    if (r == 1 || r == 4 || r == 7) {
                        if (lctList[r - 1][c] + lctList[r][c] < 1) {
                            minList = 0;
                        }
                    }
                    if (r == 2 || r == 5 || r == 8) {
                        if (lctList[r - 2][c] + lctList[r - 1][c] + lctList[r][c] > 2) {
                            sumList = 3;
                        }
                    }
                    if (r == 5) {
                        if (lctList[r - 5][c] + lctList[r - 4][c] +
                                lctList[r - 3][c] + lctList[r - 2][c] +
                                lctList[r - 1][c] + lctList[r][c] < 3) {
                            sumList = 3;
                        }
                    }
                    if (r == 6) {
                        if (lctList[r - 6][c] + lctList[r - 5][c] + lctList[r - 4][c] +
                                lctList[r - 3][c] + lctList[r - 2][c] +
                                lctList[r - 1][c] + lctList[r][c] > 4) {
                            sumList = 3;
                        }
                    }
                    if (r == 7) {
                        if (lctList[r - 7][c] + lctList[r - 6][c] +
                                lctList[r - 5][c] + lctList[r - 4][c] +
                                lctList[r - 3][c] + lctList[r - 2][c] +
                                lctList[r - 1][c] + lctList[r][c] > 5) {
                            sumPage = 6;
                        }
                    }
                    if (r == 8) {
                        if (lctList[r - 8][c] + lctList[r - 7][c] + lctList[r - 6][c] +
                                lctList[r - 5][c] + lctList[r - 4][c] +
                                lctList[r - 3][c] + lctList[r - 2][c] +
                                lctList[r - 1][c] + lctList[r][c] > 5) {
                            sumPage = 6;
                        }
                    }
                }
            } while (minList == 0 || sumList == 3 || sumPage == 6);
        }
        return lctList;
    }

    public void rdmNumList() {
        int[][] numList = new int[10][9];
        int num = 0;
        ArrayList<Integer> column;
        for (int c = 0; c < 9; c++) {
            column = rdmColumn();
            for (int r = 0; r < 10; r++) {
                num = column.get(r);
                numList[r][c] = num + (c * 10);
            }
        }
        for (int r = 0; r < 5; r++) {
            for (int c = 0; c < 9; c++) {
                numList1[r][c] = numList[r][c];
            }
        }
        for (int r = 5; r < 10; r++) {
            for (int c = 0; c < 9; c++) {
                numList2[r - 5][c] = numList[r][c];
            }
        }
    }

    public int[] rdmRow() {
        int[] rowList = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        Random rdm = new Random();
        int sum = 0;
        do {
            rowList[rdm.nextInt(9)] = 1;
            sum = 0;
            for (int i = 0; i < 9; i++) {
                sum += rowList[i];
            }
        } while (sum < 5);
        return rowList;
    }

    public ArrayList<Integer> rdmColumn() {
        boolean duplicated = false;
        Random rdm = new Random();
        ArrayList column = new ArrayList();
        int num = 0;
        for (int i = 0; i < 10; i++) {
            do {
                num = rdm.nextInt(10);
            } while (column.contains(num));
            column.add(num);
        }
        return column;
    }

    public void newModel() {
        key = 0;
        while (modelList.getModelList().containsKey(key))
            key++;
        modelKey.setText(key + "");
        showModel(key);
    }

    public void nextModel() {
        key = Integer.parseInt(modelKey.getText() + "");
        if (modelList.getModelList().size() > key)
            modelKey.setText((key += 1) + "");
        showModel(key);
    }

    public void prevModel() {
        key = Integer.parseInt(modelKey.getText() + "");
        if (key > 0)
            modelKey.setText((key -= 1) + "");
        showModel(key);
    }

    private void showModel(int key) {
        if (modelList.getModelList().containsKey(key)) {
            Model model = modelList.getModelList().get(key);
            int[][] rc = model.getModel();
            for (int r = 0; r < 9; r++) {
                for (int c = 0; c < 9; c++) {
                    if (rc[r][c] == 0)
                        textViewList[r][c].setText("");
                    else
                        textViewList[r][c].setText(rc[r][c] + "");
                }
            }
        } else {
            for (int r = 0; r < 9; r++) {
                for (int c = 0; c < 9; c++) {
                    textViewList[r][c].setText("");
                }
            }
        }
    }

    private void deleteModel() {
        key = Integer.parseInt(modelKey.getText() + "");
        modelList.getModelList().remove(key);
        ghiObject(modelList);
    }

    public void createModel() {
        key = Integer.parseInt(modelKey.getText() + "");
        if (modelList.getModelList().containsKey(key) == false) {
            int[][] rc = new int[9][9];
            Model model = new Model();
            for (int r = 0; r < 9; r++) {
                for (int c = 0; c < 9; c++) {
                    if (textViewList[r][c].getText().toString().isEmpty())
                        rc[r][c] = 0;
                    else
                        rc[r][c] = Integer.parseInt(textViewList[r][c].getText() + "");
                }
            }
            model.setModel(rc);
            modelList.addModel(key, model);
            ghiObject(modelList);
        }
    }

    public void ghiObject(ModelList modelList) {
        try {
            FileOutputStream out = openFileOutput("ListObject.bin", MODE_PRIVATE);
            ObjectOutputStream objOut = new ObjectOutputStream(out);
            objOut.writeObject(modelList);
            objOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ModelList docObject() {
        try {
            FileInputStream in = openFileInput("ListObject.bin");
            ObjectInputStream objIn = new ObjectInputStream(in);
            ModelList modelList = (ModelList) objIn.readObject();
            objIn.close();
            return modelList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelList = new ModelList();
    }

    public void setOnClick() {
        btnOpenPlayer.setOnClickListener(this);
        btnCreate.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnNew.setOnClickListener(this);
    }

    public void linkView() {
        btnOpenPlayer = (Button) findViewById(R.id.btn_openPlayer);
        btnCreate = (Button) findViewById(R.id.btn_create);
        btnDelete = (Button) findViewById(R.id.btn_delete);
        btnNext = (Button) findViewById(R.id.btn_next);
        btnPrev = (Button) findViewById(R.id.btn_prev);
        btnNew = (Button) findViewById(R.id.btn_autoCreate);
        modelKey = (TextView) findViewById(R.id.modelKey);
        textViewList[0][0] = (TextView) findViewById(R.id.edit_0_0);
        textViewList[0][1] = (TextView) findViewById(R.id.edit_0_1);
        textViewList[0][2] = (TextView) findViewById(R.id.edit_0_2);
        textViewList[0][3] = (TextView) findViewById(R.id.edit_0_3);
        textViewList[0][4] = (TextView) findViewById(R.id.edit_0_4);
        textViewList[0][5] = (TextView) findViewById(R.id.edit_0_5);
        textViewList[0][6] = (TextView) findViewById(R.id.edit_0_6);
        textViewList[0][7] = (TextView) findViewById(R.id.edit_0_7);
        textViewList[0][8] = (TextView) findViewById(R.id.edit_0_8);
        textViewList[1][0] = (TextView) findViewById(R.id.edit_1_0);
        textViewList[1][1] = (TextView) findViewById(R.id.edit_1_1);
        textViewList[1][2] = (TextView) findViewById(R.id.edit_1_2);
        textViewList[1][3] = (TextView) findViewById(R.id.edit_1_3);
        textViewList[1][4] = (TextView) findViewById(R.id.edit_1_4);
        textViewList[1][5] = (TextView) findViewById(R.id.edit_1_5);
        textViewList[1][6] = (TextView) findViewById(R.id.edit_1_6);
        textViewList[1][7] = (TextView) findViewById(R.id.edit_1_7);
        textViewList[1][8] = (TextView) findViewById(R.id.edit_1_8);
        textViewList[2][0] = (TextView) findViewById(R.id.edit_2_0);
        textViewList[2][1] = (TextView) findViewById(R.id.edit_2_1);
        textViewList[2][2] = (TextView) findViewById(R.id.edit_2_2);
        textViewList[2][3] = (TextView) findViewById(R.id.edit_2_3);
        textViewList[2][4] = (TextView) findViewById(R.id.edit_2_4);
        textViewList[2][5] = (TextView) findViewById(R.id.edit_2_5);
        textViewList[2][6] = (TextView) findViewById(R.id.edit_2_6);
        textViewList[2][7] = (TextView) findViewById(R.id.edit_2_7);
        textViewList[2][8] = (TextView) findViewById(R.id.edit_2_8);
        textViewList[3][0] = (TextView) findViewById(R.id.edit_3_0);
        textViewList[3][1] = (TextView) findViewById(R.id.edit_3_1);
        textViewList[3][2] = (TextView) findViewById(R.id.edit_3_2);
        textViewList[3][3] = (TextView) findViewById(R.id.edit_3_3);
        textViewList[3][4] = (TextView) findViewById(R.id.edit_3_4);
        textViewList[3][5] = (TextView) findViewById(R.id.edit_3_5);
        textViewList[3][6] = (TextView) findViewById(R.id.edit_3_6);
        textViewList[3][7] = (TextView) findViewById(R.id.edit_3_7);
        textViewList[3][8] = (TextView) findViewById(R.id.edit_3_8);
        textViewList[4][0] = (TextView) findViewById(R.id.edit_4_0);
        textViewList[4][1] = (TextView) findViewById(R.id.edit_4_1);
        textViewList[4][2] = (TextView) findViewById(R.id.edit_4_2);
        textViewList[4][3] = (TextView) findViewById(R.id.edit_4_3);
        textViewList[4][4] = (TextView) findViewById(R.id.edit_4_4);
        textViewList[4][5] = (TextView) findViewById(R.id.edit_4_5);
        textViewList[4][6] = (TextView) findViewById(R.id.edit_4_6);
        textViewList[4][7] = (TextView) findViewById(R.id.edit_4_7);
        textViewList[4][8] = (TextView) findViewById(R.id.edit_4_8);
        textViewList[5][0] = (TextView) findViewById(R.id.edit_5_0);
        textViewList[5][1] = (TextView) findViewById(R.id.edit_5_1);
        textViewList[5][2] = (TextView) findViewById(R.id.edit_5_2);
        textViewList[5][3] = (TextView) findViewById(R.id.edit_5_3);
        textViewList[5][4] = (TextView) findViewById(R.id.edit_5_4);
        textViewList[5][5] = (TextView) findViewById(R.id.edit_5_5);
        textViewList[5][6] = (TextView) findViewById(R.id.edit_5_6);
        textViewList[5][7] = (TextView) findViewById(R.id.edit_5_7);
        textViewList[5][8] = (TextView) findViewById(R.id.edit_5_8);
        textViewList[6][0] = (TextView) findViewById(R.id.edit_6_0);
        textViewList[6][1] = (TextView) findViewById(R.id.edit_6_1);
        textViewList[6][2] = (TextView) findViewById(R.id.edit_6_2);
        textViewList[6][3] = (TextView) findViewById(R.id.edit_6_3);
        textViewList[6][4] = (TextView) findViewById(R.id.edit_6_4);
        textViewList[6][5] = (TextView) findViewById(R.id.edit_6_5);
        textViewList[6][6] = (TextView) findViewById(R.id.edit_6_6);
        textViewList[6][7] = (TextView) findViewById(R.id.edit_6_7);
        textViewList[6][8] = (TextView) findViewById(R.id.edit_6_8);
        textViewList[7][0] = (TextView) findViewById(R.id.edit_7_0);
        textViewList[7][1] = (TextView) findViewById(R.id.edit_7_1);
        textViewList[7][2] = (TextView) findViewById(R.id.edit_7_2);
        textViewList[7][3] = (TextView) findViewById(R.id.edit_7_3);
        textViewList[7][4] = (TextView) findViewById(R.id.edit_7_4);
        textViewList[7][5] = (TextView) findViewById(R.id.edit_7_5);
        textViewList[7][6] = (TextView) findViewById(R.id.edit_7_6);
        textViewList[7][7] = (TextView) findViewById(R.id.edit_7_7);
        textViewList[7][8] = (TextView) findViewById(R.id.edit_7_8);
        textViewList[8][0] = (TextView) findViewById(R.id.edit_8_0);
        textViewList[8][1] = (TextView) findViewById(R.id.edit_8_1);
        textViewList[8][2] = (TextView) findViewById(R.id.edit_8_2);
        textViewList[8][3] = (TextView) findViewById(R.id.edit_8_3);
        textViewList[8][4] = (TextView) findViewById(R.id.edit_8_4);
        textViewList[8][5] = (TextView) findViewById(R.id.edit_8_5);
        textViewList[8][6] = (TextView) findViewById(R.id.edit_8_6);
        textViewList[8][7] = (TextView) findViewById(R.id.edit_8_7);
        textViewList[8][8] = (TextView) findViewById(R.id.edit_8_8);
    }

    public void testNumList(int[][] list) {
        System.out.println("Number");
        for (int r = 0; r < list.length; r++) {
            for (int c = 0; c < list[r].length; c++) {
                System.out.print(list[r][c] + " ");
            }
            System.out.println();
        }
    }

    public void testLocation(int[][] list) {
        System.out.println("Location");
        for (int r = 0; r < list.length; r++) {
            for (int c = 0; c < list[r].length; c++) {
                System.out.print(list[r][c] + " ");
            }
            System.out.println("dòng: " + r);
        }
        for (int c = 0; c < list.length; c++) {
            int sum = 0;
            for (int r = 0; r < list.length; r++) {
                sum += list[r][c];
            }
            System.out.print(sum + " ");
        }
        System.out.println("ok");
    }

    public void testModel(String[][] list) {
        System.out.println("Model");
        for (int r = 0; r < list.length; r++) {
            for (int c = 0; c < list[r].length; c++) {
                System.out.print(list[r][c] + " ");
            }
            System.out.println();
        }
    }
}
