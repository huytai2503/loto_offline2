package edu.khtn.loto_offline;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;

import edu.khtn.object.Player;
import edu.khtn.object.PlayerList;

public class MainActivity extends Activity
        implements Serializable, View.OnClickListener, View.OnFocusChangeListener {
    Intent intentModels, intentReader;
    EditText[] editArr = new EditText[6];
    EditText tvFirstCash, tvPrice, tvReadTime, tvPlayTime;
    Button btnOpenModels, btnOpenReader;
    PlayerList playerList = new PlayerList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intentReader = new Intent(this, ReaderActivity.class);
        intentModels = new Intent(this, ModelsActivity.class);
        linkAllViewAndSetListener();
    }

    @Override
    public void onClick(View v) {
        if (v == btnOpenReader)
            openReader();
        if (v == btnOpenModels)
            startActivity(intentModels);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v == tvPrice) {
            if (isNotNumber(tvPrice.getText().toString())) {
                toast(getString(R.string.alert_isNotNumber));
                tvPrice.setText("");
            }
        }
        if (v == tvReadTime) {
            if (isNotNumber(tvReadTime.getText().toString())) {
                toast(getString(R.string.alert_isNotNumber));
                tvReadTime.setText("");
            }
        }
        if (v == tvPlayTime) {
            if (isNotNumber(tvPlayTime.getText().toString())) {
                toast(getString(R.string.alert_isNotNumber));
                tvPlayTime.setText("");
            }
        }
        if (v == tvFirstCash) {
            if (isNotNumber(tvFirstCash.getText().toString())) {
                toast(getString(R.string.alert_isNotNumber));
                tvFirstCash.setText("");
            }
        }
    }

    public void openReader() {
        String sReadTime = tvReadTime.getText().toString();
        String sPlayTime = tvPlayTime.getText().toString();
        String sPricePer = tvPrice.getText().toString();
        if (sReadTime.isEmpty() || sPlayTime.isEmpty())
            Toast.makeText(getBaseContext(), getString
                    (R.string.alert_input), Toast.LENGTH_SHORT).show();
        else {
            String name;
            int cash;
            for (int i = 0; i < 6; i++) {
                if (editArr[i].getText().toString().isEmpty() == false) {
                    Player player = new Player();
                    name = editArr[i].getText().toString();
                    cash = Integer.parseInt(tvFirstCash.getText().toString());
                    player.setName(name);
                    player.setCash(cash);
                    playerList.addPlayer(player);
                }
            }
            intentReader.putExtra("Price", sPricePer);
            intentReader.putExtra("ReadTime", sReadTime);
            intentReader.putExtra("PlayTime", sPlayTime);
            intentReader.putExtra("playerList", playerList);
            startActivity(intentReader);
        }
    }

    public void linkAllViewAndSetListener() {
        btnOpenModels = (Button) findViewById(R.id.btn_openModels);
        btnOpenModels.setOnClickListener(this);
        btnOpenReader = (Button) findViewById(R.id.btn_openReader);
        btnOpenReader.setOnClickListener(this);
        tvFirstCash = (EditText) findViewById(R.id.edit_firstCash);
        tvFirstCash.setOnFocusChangeListener(this);
        tvPrice = (EditText) findViewById(R.id.edit_pricePer);
        tvPrice.setOnFocusChangeListener(this);
        tvReadTime = (EditText) findViewById(R.id.edit_readTime);
        tvReadTime.setOnFocusChangeListener(this);
        tvPlayTime = (EditText) findViewById(R.id.edit_playTime);
        tvPlayTime.setOnFocusChangeListener(this);
        editArr[0] = (EditText) findViewById(R.id.edit_name1);
        editArr[1] = (EditText) findViewById(R.id.edit_name2);
        editArr[2] = (EditText) findViewById(R.id.edit_name3);
        editArr[3] = (EditText) findViewById(R.id.edit_name4);
        editArr[4] = (EditText) findViewById(R.id.edit_name5);
        editArr[5] = (EditText) findViewById(R.id.edit_name6);
    }

    public void toast(String msg) {
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public boolean isNotNumber(String number) {
        try {
            Integer.parseInt(number);
            return false;
        } catch (Exception e) {
        }
        return true;
    }

}
