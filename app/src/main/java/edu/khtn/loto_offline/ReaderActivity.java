package edu.khtn.loto_offline;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.Serializable;
import java.util.Random;

import edu.khtn.object.PlayerList;

public class ReaderActivity extends Activity
        implements Serializable, View.OnClickListener {
    FrameLayout buttonLayout;
    TextView[][] textViewList = new TextView[9][10];
    TextView[] tvNameList = new TextView[6];
    TextView[] tvCashList = new TextView[6];
    TextView tvPrice, tvReadTime, tvPlayTime, tvReader, tvTimeRemain;
    Button btnRead, btnPause;
    CountDownTimer timer;
    MediaPlayer player = new MediaPlayer();
    int[] numList = new int[90];
    int currentSum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);
        linkViewAndSetOnClick();
        getIntentFromMainLayout();
    }

    @Override
    protected void onDestroy() {
        if (timer != null)
            timer.cancel();
        player.release();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v == btnRead) {
            createCountdownAndRead();
        }
        if (v == btnPause) {
            String nameButton = btnPause.getText().toString();
            if (nameButton.contentEquals(getString(R.string.pause_reading))) {
                timer.cancel();
                btnPause.setText(getText(R.string.continue_reading));
            } else {
                timer.start();
                btnPause.setText(getText(R.string.pause_reading));
            }
        }
    }

    public void createCountdownAndRead() {
        btnRead.setEnabled(false);
        btnPause = new Button(this);
        btnPause.setText(getText(R.string.pause_reading));
        btnPause.setOnClickListener(this);
        buttonLayout.removeAllViews();
        buttonLayout.addView(btnPause);
        String getInterval = tvReadTime.getText().toString();
        String getPlayTime = tvPlayTime.getText().toString();
        final int interval = Integer.parseInt(getInterval);
        final int minutes = Integer.parseInt(getPlayTime);
        timer = new CountDownTimer(minutes * 60 * 1000, interval * 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                if (currentSum < 4095) {
                    int rdm = randomAndAddToNumList();
                    setNumberToGridView(rdm);
                    do {
                        readMp3File(rdm);
                    } while (!player.isPlaying());
                } else {
                    toast(getString(R.string.alert_finish));
                    player.release();
                    timer.cancel();
                    finish();
                }
            }

            @Override
            public void onFinish() {
                toast(getString(R.string.alert_outTime));
                player.release();
                timer.cancel();
                finish();
            }
        }.start();
    }

    public int randomAndAddToNumList() {
        int num = 0;
        currentSum = 0;
        Random rdm = new Random();
        if (currentSum < 4095) {
            do {
                num = rdm.nextInt(90) + 1;
            } while (num == numList[num - 1]);
            numList[num - 1] = num;
            for (int i = 0; i < 90; i++) {
                currentSum += numList[i];
            }
        }
        return num;
    }

    public void setNumberToGridView(int rdm) {
        tvReader.setText(rdm + "");
        if (rdm < 10) {
            textViewList[(rdm - 1) / 10][(rdm - 1) % 10].
                    setText("  " + rdm + "");
        } else {
            textViewList[(rdm - 1) / 10][(rdm - 1) % 10].
                    setText(" " + rdm + "");
        }
    }

    public void readMp3File(int rdm) {
        try {
            player.release();
            player = new MediaPlayer();
            AssetFileDescriptor asset =
                    getAssets().openFd(rdm + ".mp3");
            player.setDataSource(asset.getFileDescriptor(),
                    asset.getStartOffset(), asset.getLength());
            player.prepare();
            player.start();
            asset.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getIntentFromMainLayout() {
        Intent intentResult = getIntent();
        tvPrice.setText(intentResult.getStringExtra("Price"));
        tvReadTime.setText(intentResult.getStringExtra("ReadTime"));
        tvPlayTime.setText(intentResult.getStringExtra("PlayTime"));
        PlayerList playerList = (PlayerList)
                intentResult.getSerializableExtra("playerList");
        for (int i = 0; i < playerList.size(); i++) {
            String name = playerList.getPlayerList().get(i).getName();
            String cash = String.valueOf
                    (playerList.getPlayerList().get(i).getCash());
            tvNameList[i].setText(name + ": ");
            tvCashList[i].setText(cash);
        }
    }

    public void linkViewAndSetOnClick() {
        buttonLayout = (FrameLayout) findViewById(R.id.button_layout);
        btnRead = (Button) findViewById(R.id.btn_read);
        btnRead.setOnClickListener(this);
        tvTimeRemain = (TextView) findViewById(R.id.time_remain);
        tvPrice = (TextView) findViewById(R.id.text_price);
        tvReadTime = (TextView) findViewById(R.id.text_readTime);
        tvPlayTime = (TextView) findViewById(R.id.text_playTime);
        tvReader = (TextView) findViewById(R.id.text_reader);
        tvNameList[0] = (TextView) findViewById(R.id.text_name1);
        tvNameList[1] = (TextView) findViewById(R.id.text_name2);
        tvNameList[2] = (TextView) findViewById(R.id.text_name3);
        tvNameList[3] = (TextView) findViewById(R.id.text_name4);
        tvNameList[4] = (TextView) findViewById(R.id.text_name5);
        tvNameList[5] = (TextView) findViewById(R.id.text_name6);
        tvCashList[0] = (TextView) findViewById(R.id.text_cash1);
        tvCashList[1] = (TextView) findViewById(R.id.text_cash2);
        tvCashList[2] = (TextView) findViewById(R.id.text_cash3);
        tvCashList[3] = (TextView) findViewById(R.id.text_cash4);
        tvCashList[4] = (TextView) findViewById(R.id.text_cash5);
        tvCashList[5] = (TextView) findViewById(R.id.text_cash6);
        textViewList[0][0] = (TextView) findViewById(R.id.text_0_0);
        textViewList[0][1] = (TextView) findViewById(R.id.text_0_1);
        textViewList[0][2] = (TextView) findViewById(R.id.text_0_2);
        textViewList[0][3] = (TextView) findViewById(R.id.text_0_3);
        textViewList[0][4] = (TextView) findViewById(R.id.text_0_4);
        textViewList[0][5] = (TextView) findViewById(R.id.text_0_5);
        textViewList[0][6] = (TextView) findViewById(R.id.text_0_6);
        textViewList[0][7] = (TextView) findViewById(R.id.text_0_7);
        textViewList[0][8] = (TextView) findViewById(R.id.text_0_8);
        textViewList[0][9] = (TextView) findViewById(R.id.text_0_9);
        textViewList[1][0] = (TextView) findViewById(R.id.text_1_0);
        textViewList[1][1] = (TextView) findViewById(R.id.text_1_1);
        textViewList[1][2] = (TextView) findViewById(R.id.text_1_2);
        textViewList[1][3] = (TextView) findViewById(R.id.text_1_3);
        textViewList[1][4] = (TextView) findViewById(R.id.text_1_4);
        textViewList[1][5] = (TextView) findViewById(R.id.text_1_5);
        textViewList[1][6] = (TextView) findViewById(R.id.text_1_6);
        textViewList[1][7] = (TextView) findViewById(R.id.text_1_7);
        textViewList[1][8] = (TextView) findViewById(R.id.text_1_8);
        textViewList[1][9] = (TextView) findViewById(R.id.text_1_9);
        textViewList[2][0] = (TextView) findViewById(R.id.text_2_0);
        textViewList[2][1] = (TextView) findViewById(R.id.text_2_1);
        textViewList[2][2] = (TextView) findViewById(R.id.text_2_2);
        textViewList[2][3] = (TextView) findViewById(R.id.text_2_3);
        textViewList[2][4] = (TextView) findViewById(R.id.text_2_4);
        textViewList[2][5] = (TextView) findViewById(R.id.text_2_5);
        textViewList[2][6] = (TextView) findViewById(R.id.text_2_6);
        textViewList[2][7] = (TextView) findViewById(R.id.text_2_7);
        textViewList[2][8] = (TextView) findViewById(R.id.text_2_8);
        textViewList[2][9] = (TextView) findViewById(R.id.text_2_9);
        textViewList[3][0] = (TextView) findViewById(R.id.text_3_0);
        textViewList[3][1] = (TextView) findViewById(R.id.text_3_1);
        textViewList[3][2] = (TextView) findViewById(R.id.text_3_2);
        textViewList[3][3] = (TextView) findViewById(R.id.text_3_3);
        textViewList[3][4] = (TextView) findViewById(R.id.text_3_4);
        textViewList[3][5] = (TextView) findViewById(R.id.text_3_5);
        textViewList[3][6] = (TextView) findViewById(R.id.text_3_6);
        textViewList[3][7] = (TextView) findViewById(R.id.text_3_7);
        textViewList[3][8] = (TextView) findViewById(R.id.text_3_8);
        textViewList[3][9] = (TextView) findViewById(R.id.text_3_9);
        textViewList[4][0] = (TextView) findViewById(R.id.text_4_0);
        textViewList[4][1] = (TextView) findViewById(R.id.text_4_1);
        textViewList[4][2] = (TextView) findViewById(R.id.text_4_2);
        textViewList[4][3] = (TextView) findViewById(R.id.text_4_3);
        textViewList[4][4] = (TextView) findViewById(R.id.text_4_4);
        textViewList[4][5] = (TextView) findViewById(R.id.text_4_5);
        textViewList[4][6] = (TextView) findViewById(R.id.text_4_6);
        textViewList[4][7] = (TextView) findViewById(R.id.text_4_7);
        textViewList[4][8] = (TextView) findViewById(R.id.text_4_8);
        textViewList[4][9] = (TextView) findViewById(R.id.text_4_9);
        textViewList[5][0] = (TextView) findViewById(R.id.text_5_0);
        textViewList[5][1] = (TextView) findViewById(R.id.text_5_1);
        textViewList[5][2] = (TextView) findViewById(R.id.text_5_2);
        textViewList[5][3] = (TextView) findViewById(R.id.text_5_3);
        textViewList[5][4] = (TextView) findViewById(R.id.text_5_4);
        textViewList[5][5] = (TextView) findViewById(R.id.text_5_5);
        textViewList[5][6] = (TextView) findViewById(R.id.text_5_6);
        textViewList[5][7] = (TextView) findViewById(R.id.text_5_7);
        textViewList[5][8] = (TextView) findViewById(R.id.text_5_8);
        textViewList[5][9] = (TextView) findViewById(R.id.text_5_9);
        textViewList[6][0] = (TextView) findViewById(R.id.text_6_0);
        textViewList[6][1] = (TextView) findViewById(R.id.text_6_1);
        textViewList[6][2] = (TextView) findViewById(R.id.text_6_2);
        textViewList[6][3] = (TextView) findViewById(R.id.text_6_3);
        textViewList[6][4] = (TextView) findViewById(R.id.text_6_4);
        textViewList[6][5] = (TextView) findViewById(R.id.text_6_5);
        textViewList[6][6] = (TextView) findViewById(R.id.text_6_6);
        textViewList[6][7] = (TextView) findViewById(R.id.text_6_7);
        textViewList[6][8] = (TextView) findViewById(R.id.text_6_8);
        textViewList[6][9] = (TextView) findViewById(R.id.text_6_9);
        textViewList[7][0] = (TextView) findViewById(R.id.text_7_0);
        textViewList[7][1] = (TextView) findViewById(R.id.text_7_1);
        textViewList[7][2] = (TextView) findViewById(R.id.text_7_2);
        textViewList[7][3] = (TextView) findViewById(R.id.text_7_3);
        textViewList[7][4] = (TextView) findViewById(R.id.text_7_4);
        textViewList[7][5] = (TextView) findViewById(R.id.text_7_5);
        textViewList[7][6] = (TextView) findViewById(R.id.text_7_6);
        textViewList[7][7] = (TextView) findViewById(R.id.text_7_7);
        textViewList[7][8] = (TextView) findViewById(R.id.text_7_8);
        textViewList[7][9] = (TextView) findViewById(R.id.text_7_9);
        textViewList[8][0] = (TextView) findViewById(R.id.text_8_0);
        textViewList[8][1] = (TextView) findViewById(R.id.text_8_1);
        textViewList[8][2] = (TextView) findViewById(R.id.text_8_2);
        textViewList[8][3] = (TextView) findViewById(R.id.text_8_3);
        textViewList[8][4] = (TextView) findViewById(R.id.text_8_4);
        textViewList[8][5] = (TextView) findViewById(R.id.text_8_5);
        textViewList[8][6] = (TextView) findViewById(R.id.text_8_6);
        textViewList[8][7] = (TextView) findViewById(R.id.text_8_7);
        textViewList[8][8] = (TextView) findViewById(R.id.text_8_8);
        textViewList[8][9] = (TextView) findViewById(R.id.text_8_9);
    }

    public void toast(String msg) {
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }

}
